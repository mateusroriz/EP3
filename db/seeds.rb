# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create(
email: "admin@example.com",
password: "12345678",
password_confirmation: "12345678")



Message.create(
	title: "Welcome",
	description: "In this Message Board you are allowed to create messages and comment in your and other user messages, feel free to use it as you wish.",
	user_id: 1

	)